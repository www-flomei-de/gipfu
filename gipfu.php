<?php

namespace flomei;

class GetInstagramPostsForUser
{
    private $username = '';
    private $data_from_instagram;
    
    # Set username, so the script knows which profile to look at
    public function setUsername($username)
    {
        if (strlen($username) > 0) {
            $this->username = $username;
        } else {
            throw new \Exception('setUsername can not run. Username needs to be longer than 0 chars.');
        }
    }

    # Get all data that was obtained from Instagram
    # This function will not do any processing on the data whatsoever
    public function getUserFullData($asJSON = false)
    {
        if ($this->isUserSet()) {
            $this->getData();

            $working_data = json_decode($this->data_from_instagram);
          
            if ($asJSON) {
                $working_data = json_encode($working_data);
            }

            return $working_data;
        } else {
            throw new \Exception('getUserFullData can not run. Username is not set. Call setUsername first.');
        }
    }

    # Get user information like username, follower and post count and more
    public function getUserInfo($asJSON = false)
    {
        if ($this->isUserSet()) {
            $this->getData();
              
            $working_data = json_decode($this->data_from_instagram);
            $working_data = $working_data->entry_data->ProfilePage[0]->graphql->user;

            $user_info['username'] = $working_data->username;
            $user_info['full_name'] = $working_data->full_name;
            $user_info['biography'] = $working_data->biography;
            $user_info['follower'] = $working_data->edge_followed_by->count;
            $user_info['follows'] = $working_data->edge_follow->count;
            $user_info['posts'] = $working_data->edge_owner_to_timeline_media->count;
            $user_info['external_url'] = $working_data->external_url;
            $user_info['external_url_insta'] = $working_data->external_url_linkshimmed;
            $user_info['profile_picture'] = $working_data->profile_pic_url;
            $user_info['profile_picture_hd'] = $working_data->profile_pic_url_hd;
            $user_info['id'] = $working_data->id;
            $user_info['highlight_reel_count'] = $working_data->highlight_reel_count;
            $user_info['is_private'] = $working_data->is_private;
            $user_info['is_verified'] = $working_data->is_verified;
            $user_info['is_business_account'] = $working_data->is_business_account;
            $user_info['is_joined_recently'] = $working_data->is_joined_recently;
            $user_info['business_category_name'] = $working_data->business_category_name;
            $user_info['connected_fb_page'] = $working_data->connected_fb_page;
              
            if ($asJSON) {
                $user_info = json_encode($user_info);
            }

            return $user_info;
        } else {
            throw new \Exception('getUserInfo can not run. Username is not set. Call setUsername first.');
        }
    }
    
    # Get the 12 latest user posts with some specific information
    # Due to limitations while fetching posts it's not (yet) possible to obtain more
    # than the latest 12 posts - use this data for create embeds of posts for example
    public function getUserPosts($asJSON = false)
    {
        if ($this->isUserSet()) {
            $this->getData();
              
            $working_data = json_decode($this->data_from_instagram);
            $working_data = $working_data->entry_data->ProfilePage[0]->graphql->user;
            $posts = $working_data->edge_owner_to_timeline_media->edges;
            
            foreach ($posts as $post) {
                $newItem = new \stdClass();
                $newItem->id = $post->node->id;
                $newItem->shortcode = $post->node->shortcode;
                $newItem->type = $post->node->__typename;
                $newItem->is_video = $post->node->is_video;
                $newItem->caption = $post->node->edge_media_to_caption->edges->node->text;
                $newItem->accessibility_caption = $post->node->accessibility_caption;
                $newItem->comments = $post->node->edge_media_to_comment->count;
                $newItem->comments_disabled = $post->node->comments_disabled;
                $newItem->likes = $post->node->edge_liked_by->count;
                $newItem->taken_at = $post->node->taken_at_timestamp;
                $newItem->location = new \stdClass();
                $newItem->location->id = $post->node->location->id;
                $newItem->location->has_public_page = $post->node->location->has_public_page;
                $newItem->location->name = $post->node->location->name;
                $newItem->location->slug = $post->node->location->slug;
                $newItem->media = new \stdClass();
                $newItem->media->dimensions = new \stdClass();
                $newItem->media->dimensions->width = $post->node->dimensions->width;
                $newItem->media->dimensions->height = $post->node->dimensions->height;
                $newItem->media->url = $post->node->display_url;
                $newItem->media->thumbnail = new \stdClass();
                $newItem->media->thumbnail->full = $post->node->thumbnail_src;
              
                foreach ($post->node->thumbnail_resources as $thumb) {
                    $size = $thumb->config_width;
                    $newItem->media->thumbnail->$size = $thumb->src;
                }

                $user_posts[] = $newItem;
            }

            if ($asJSON) {
                $user_posts = json_encode($user_posts);
            }

            return $user_posts;
        } else {
            throw new \Exception('getUserPosts can not run. Username is not set. Call setUsername first.');
        }
    }

    # Retrieve data from Instagram via a simple file_get_contents() which seems to work fine (for the moment)
    private function getData()
    {
        if (function_exists('file_get_contents')) {
            if ($this->isUserSet()) {
                $profile_url = 'https://www.instagram.com/' . $this->username;
            } else {
                throw new \Exception('Username is not set. Call setUsername first.');
            }

            try {
                # Get the profile page
                $page_content = file_get_contents($profile_url);

                # Use the built-in functions of PHP to parse the DOM of the page
                libxml_use_internal_errors(true);
                $doc = new \DOMDocument();
                $doc->strictErrorChecking = false;
                $doc->loadHTML($page_content);
                # We know that the data is inside of a script element, so lets filter for that
                $scripts = $doc->getElementsByTagName('script');

                foreach ($scripts as $script) {
                    # For the moment this is a safe identifier of the payload we are interested in
                    # Instagram uses this to prefill their page with the first posts until lazy loading kicks in
                    if (substr($script->nodeValue, 0, 18) === 'window._sharedData') {
                        $this->data_from_instagram = $script->nodeValue;
                    }
                }

                # First 21 characters are initialization of JSON object
                $working_data = substr($this->data_from_instagram, 21);
                # Also cut off the last semicolon from the end to retrieve a valid JSON object
                $this->data_from_instagram = substr($working_data, 0, -1);

                # At this point we should have some data. otherwise throw an exception
                if (!(strlen($this->data_from_instagram) > 0)) {
                    throw new \Exception('Tried to get data but result is empty. Something went wrong.');
                }
            } catch (Exception $e) {
                throw $e;
            } finally {
                # Nothing to do here for the moment
            }
        } else {
            throw new \Exception('file_get_contents not available. Script can not run.');
        }
    }

    # Internal function to check whether a username was already set or not
    private function isUserSet()
    {
        return (boolean)(strlen($this->username) > 0);
    }
}
