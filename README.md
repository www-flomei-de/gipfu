# PHP Class "Get Instagram Posts For User", short "GIPFU"

## What does this class do?
This class provides a wrapper and a few functions to fetch basic data from Instagram without authenticating through oAuth and alike. Due to the missing authentication, this class can only do read operations and is limited on the amount of datasets that can be obtained.

Nevertheless, if you are only looking into obtaining basic information like follower counts or creating a preview for the latest posts, this class might just be what you were looking for. It's easy to use but provides all necessary information.

Due to the nature of Instagram and how the platform works in general, this class can only be used to fetched information from publicly available profiles. Private profiles might return wrong or no data at all, so make sure you are only fetching data from profiles which can be accessed publicly.

## Intended use
While you could theoretically use the information obtained through this class for direct use in your applications, like directly embedding media, you should stick to "the rules" and create Instagram embeds with the data you obtained. Obviously I can't tell you what to do and what not to, but be aware that Instagram might block your access to their platform or media, when using their platform and content against their rules.

I would recommend to have a look at [the section about embedding posts from the Instagram Developer Documentation][0] for learning how to create embeds and more from the obtained data.

[0]:https://www.instagram.com/developer/embedding/

## A word of warning
While I could not find any throttling measures by Instagram during developing and testing the class, they could implement any measures at any time. So please use this class responsible, cache results where possible and don't DDOS their endpoints. 

I have set up some watchdogs to monitor for implementation of throttling mechanisms or completely blocking this script, but I might not get a 100% coverage. So if you stumble into problems like this, feel free to report them to me.

## Installation

* Copy the `gipfu.php` to your project folder
* See the enclosed `index.php` for some sample function calls

## Usage

* Add `require '/path/to/gipfu.php';` to your PHP script, set the correct path
* Create a new instance of the class like this: `$insta = new flomei\GetInstagramPostsForUser();`, watch out for the namespace
* Set the username by utilizing the `setUsername` method like this: `$insta->setUsername('instagram');` where "instagram" is the username you are looking for
* Further functions can now be used

## Function reference

### setUsername
**Description**

Set the username whose content/information you want to fetch. This function needs to be called first after initializing an instance of the class, so following function calls can obtain data for a specific user. Failure to do so will result in exceptions.

**Parameters**

Name | Type | Required | Description
--- | --- | --- | ---
`username` | String | Yes | This value will specify whose user data the script will try to fetch. It's a mandatory value and will lead to exceptions when not set.

**Return values**

None.

### getUserFullData
**Description**

Returns the full dataset that was obtained by Instagram. 

**Parameters**

Name | Type | Required | Description
--- | --- | --- | ---
`asJSON` | Boolean | No | Whether you want the data to be returned as a JSON or not. Default value: false

**Return values**

Please have a look at the returned dataset for yourself as this is somewhat larger than the broken-down versions the are returned by the other functions. This is raw output from Instagram. Run tje enclosed `index.php` to see the return of this function

Data is returned as an JSON object when `asJSON` is set to true, otherwise as an PHP object.

### getUserInfo
**Description**

Returns information about the user.

**Parameters**

Name | Type | Required | Description
--- | --- | --- | ---
`asJSON` | Boolean | No | Whether you want the data to be returned as a JSON or not. Default value: false

**Return values**

Data is returned as an JSON object when `asJSON` is set to true, otherwise as an PHP object.

Name | Description
--- | ---
`username` | Returns the username of the queried user, although this should be known
`full_name` | Full name as specified by the user
`biography` | The biography short text
`follower` | The follower count as an integer
`follows` | The number of profiles the user is following
`posts` | Number of posts by the user
`external_url` | External URL, when one is defined, raw output without any Instagram tracking link
`external_url_insta` | External URL, attached to some Instagram tracking link
`profile_picture` | URL to the profile picture
`profile_picture_hd` | URL to the profile picture in high resolution
`id` | ID of the user account
`highlight_reel_count` | - unclear - 
`is_private` | Whether or not the profile is private. This information might be misleading, as private profiles normally don't return any information at all. Use on your own risk
`is_verified` | Will be true if the profile is verified ("blue checkmark" icon)
`is_business_account` | Whether or not this account is a business account
`is_joined_recently` | - unclear when or how long this is set -
`business_category_name` | - unclear -
`connected_fb_page` | - unclear, although seems to indicate a connection to an existing Facebook page -


### getUserPosts
**Description**

Returns the 12 latest posts from the user. 12 is at this point a fixed number, as Instagram is using lazy loading and will not output more than 12 posts in their initial dataset.

**Parameters**

Name | Type | Required | Description
--- | --- | --- | ---
`asJSON` | Boolean | No | Whether you want the data to be returned as a JSON or not. Default value: false

**Return values**

The returned dataset will contain the following fields for each of the returned post objects.

Data is returned as an JSON object when `asJSON` is set to true, otherwise as an PHP object.

Name | Description
--- | ---
`id`| ID of the post
`shortcode`| Shortcode of the post, can be used for embeds
`type`| Returns the type of the post. "GraphImage" for image posts, "GraphSidecar" for slideshow, "GraphVideo" for videos
`is_video`| Set to "1" when `type` is "GraphVideo"
`caption`| Text the user entered as a caption for this post
`accessibility_caption`| Auto generated text (by Instagram) which tries to describe the content of the image/video
`comments`| Number of comments
`comments_disabled`| Whether or not comments are disabled
`likes`| Number of likes
`taken_at`| Post publish date as UNIX timestamp
`location`| Location of this post as marked by the user with the following sub elements:
&nbsp;&nbsp;&nbsp;&nbsp;`id`| ID of the location
&nbsp;&nbsp;&nbsp;&nbsp;`has_public_page`| Whether the location has its own Instagram page
&nbsp;&nbsp;&nbsp;&nbsp;`name`| Name of the location
&nbsp;&nbsp;&nbsp;&nbsp;`slug`| Slug of the location
`media`| Information about the post media
&nbsp;&nbsp;&nbsp;&nbsp;`dimensions`| Dimenions of the post media with the following sub elements
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`width`| Width of the media
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`height`| Height of the media
&nbsp;&nbsp;&nbsp;&nbsp;`url`| URL to the media file, although this is just a JPG for video files, too 
&nbsp;&nbsp;&nbsp;&nbsp;`thumbnail`| Thumbnail images for the post, with the following sub elements
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`full`| Maximum resolution, might vary depending on media type
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`150`| Same as above, but 150 Pixel width, height may vary
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`240`| Same as above, but 240 Pixel width, height may vary
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`320`| Same as above, but 320 Pixel width, height may vary
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`480`| Same as above, but 480 Pixel width, height may vary
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`640`| Same as above, but 640 Pixel width, height may vary