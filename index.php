<?php

# You will need to require the class file for further use
require 'gipfu.php';

# Create a new instance of the class, watch out for the namespace!
$insta = new flomei\GetInstagramPostsForUser();

# First and foremost set the username, otherwise the script will return errors
# For demo purposes, make it an option to pass a username via URL parameter
if (isset($_GET['u'])) {
    $insta->setUsername($_GET['u']);
} else {
    $insta->setUsername('instagram');
}

# You can now use the following functions:
# getUserInfo() - returns user specific info
# getUserPosts() - returns the latest 12 posts of the user
# getUserFullData() - returns the full dataset that was obtained from Instagram
#
# Optional argument for those is functions is $asJSON which will return data as JSON when set
# Example: getUserInfo(true); will return user info as JSON
#
# Functions will throw exceptions when an error happens, so make sure to use try-catch-constructs to handle them
#
# Attention: All of this only works with public profiles!

echo '<pre>';

try {
    echo '### getUserInfo()

    ';
    print_r($insta->getUserInfo());
    
    echo '
    
    ### getUserPosts()
    
    ';
    print_r($insta->getUserPosts());
    
    echo '
    
    ### getUserFullData()
    
    ';
    print_r($insta->getUserFullData());
} catch (Exception $e) {
    # When an exception occurs, throw it to the users, so we can have a look at it
    throw $e;
} finally {
    # This will run, no matter whether we received an error or not
    echo '</pre>';
}
